package com.rsm.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rsm.config.SampleConfig;
import com.rsm.service.sample.SampleService;

@RestController
@RequestMapping("/api")
public class SampleController {
	
	@Autowired
	private SampleService sampleService;
	
	@Autowired
	private SampleConfig sampleConfig;
	
	@RequestMapping("/sample")
	public String sample() {
		String rtnStr = 
//		"RSM Rest API Server"; 
				sampleService.getName() + "(" + sampleConfig.getName() + ")";
		return rtnStr;
	}
}
