package com.rsm.service.sample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rsm.repository.sample.SampleRepository;


@Service
public class SampleService {
	
	@Autowired
	private SampleRepository sampleRepository;
	
	public String getName() {
		return sampleRepository.getName();
	}
}
