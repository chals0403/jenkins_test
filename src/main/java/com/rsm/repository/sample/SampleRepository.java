package com.rsm.repository.sample;

import org.springframework.stereotype.Repository;

@Repository
public interface SampleRepository {
	String getName();
}
