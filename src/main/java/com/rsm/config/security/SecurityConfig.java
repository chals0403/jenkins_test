package com.rsm.config.security;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	final protected String ADMIN = "ADMIN";

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring()
        	.antMatchers("/static/**")
        	.antMatchers("/assets/**");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
			.antMatchers("/admin/**").hasRole(ADMIN)
			.antMatchers("/**").permitAll();
		
	}
	
}
