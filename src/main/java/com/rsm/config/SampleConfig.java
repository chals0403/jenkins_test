package com.rsm.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Configuration
@ConfigurationProperties(prefix = SampleConfig.PREFIX)
@Data
public class SampleConfig {
	
	public static final String PREFIX = "sample";
	
	String name;
}
